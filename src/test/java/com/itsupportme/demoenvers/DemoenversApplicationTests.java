package com.itsupportme.demoenvers;

import com.itsupportme.demoenvers.component.AuditEntity;
import com.itsupportme.demoenvers.component.AuditService;
import com.itsupportme.demoenvers.model.User;
import com.itsupportme.demoenvers.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsCollectionContaining.hasItem;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoenversApplicationTests {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AuditService testSubject;

    @Test
    public void enversGetChangedFields_shouldReturnChangedFields() {

        //given
        User user = new User()
                .setEmail("test@mail.com")
                .setFirstName("test")
                .setLastName("test");

        User savedUser = userRepository.save(user);

        userRepository.save(savedUser.setEmail("changeEmail@mail.com"));

        //when
        Collection<AuditEntity<User>> result = testSubject.findChanges(User.class, savedUser.getId());

        //then
        assertThat(
                result.stream().findFirst().map(AuditEntity::getChangedFields).orElseThrow(RuntimeException::new),
                hasItem("email"));
    }
}