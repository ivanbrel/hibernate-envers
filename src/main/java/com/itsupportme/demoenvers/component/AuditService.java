package com.itsupportme.demoenvers.component;

import java.util.Collection;

public interface AuditService {
    <T> Collection<AuditEntity<T>> findChanges(Class<T> cls, Object id);
}
