package com.itsupportme.demoenvers.component;

import com.itsupportme.demoenvers.model.UserRevisionEntity;
import org.hibernate.envers.RevisionType;

import java.util.Set;

public class AuditEntity<T> {

    private final UserRevisionEntity revision;
    private final RevisionType type;
    private final T entity;
    private final Set<String> changedFields;

    public AuditEntity(UserRevisionEntity revision, RevisionType type, T entity, Set<String> changedFields) {
        this.revision = revision;
        this.type = type;
        this.entity = entity;
        this.changedFields = changedFields;
    }

    public UserRevisionEntity getRevision() {
        return revision;
    }

    public RevisionType getType() {
        return type;
    }

    public T getEntity() {
        return entity;
    }

    public Set<String> getChangedFields() {
        return changedFields;
    }
}
