package com.itsupportme.demoenvers.component;

import com.itsupportme.demoenvers.model.User;
import com.itsupportme.demoenvers.model.UserRevisionEntity;
import com.itsupportme.demoenvers.repository.UserRepository;
import org.hibernate.envers.RevisionListener;

public class UserRevisionListener implements RevisionListener {

    @Override
    public void newRevision(Object revisionEntity) {
        UserRevisionEntity userRevisionEntity = (UserRevisionEntity) revisionEntity;
        userRevisionEntity.setUsername("test");
        userRevisionEntity.setUserId(1);
    }
}

