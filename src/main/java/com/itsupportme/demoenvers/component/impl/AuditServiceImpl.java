package com.itsupportme.demoenvers.component.impl;

import com.itsupportme.demoenvers.component.AuditEntity;
import com.itsupportme.demoenvers.component.AuditEntityFactory;
import com.itsupportme.demoenvers.component.AuditService;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.RevisionType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@Transactional
public class AuditServiceImpl implements AuditService {

    @PersistenceContext
    private EntityManager entityManager;
    private final AuditEntityFactory auditEntityFactory;

    public AuditServiceImpl(AuditEntityFactory auditEntityFactory) {
        this.auditEntityFactory = auditEntityFactory;
    }

    @Override
    public <T> Collection<AuditEntity<T>> findChanges(Class<T> cls, Object id) {
        List results = AuditReaderFactory.get(entityManager)
                .createQuery().forRevisionsOfEntityWithChanges(cls, false)
                .add(org.hibernate.envers.query.AuditEntity.id().eq(id))
                .getResultList();

        return (List<AuditEntity<T>>) results
                .stream()
                .filter(castAndTest())
                .map(it -> auditEntityFactory.create(cls, (Object[]) it))
                .collect(Collectors.toList());
    }

    private Predicate<Object> castAndTest() {
        return object -> {
            Object[] objects = (Object[]) object;
            return objects[2].equals(RevisionType.MOD);
        };
    }
}
