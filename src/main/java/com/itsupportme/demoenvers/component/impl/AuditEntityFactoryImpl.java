package com.itsupportme.demoenvers.component.impl;

import com.itsupportme.demoenvers.component.AuditEntity;
import com.itsupportme.demoenvers.component.AuditEntityFactory;
import com.itsupportme.demoenvers.model.UserRevisionEntity;
import org.hibernate.envers.RevisionType;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public final class AuditEntityFactoryImpl implements AuditEntityFactory {

    @Override
    public <T> AuditEntity<T> create(Class<T> cls, Object[] args) {
        return new AuditEntity<T>((UserRevisionEntity) args[1], (RevisionType) args[2], cls.cast(args[0]), (Set<String>) args[3]);
    }
}
