package com.itsupportme.demoenvers.component;

public interface AuditEntityFactory {
    <T> AuditEntity<T> create(Class<T> cls, Object[] args);
}
