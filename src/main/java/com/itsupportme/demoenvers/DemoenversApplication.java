package com.itsupportme.demoenvers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoenversApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoenversApplication.class, args);
	}
}
