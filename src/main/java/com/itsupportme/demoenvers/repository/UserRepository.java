package com.itsupportme.demoenvers.repository;

import com.itsupportme.demoenvers.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.history.RevisionRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}
