package com.itsupportme.demoenvers.repository;

import com.itsupportme.demoenvers.model.Event;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.history.RevisionRepository;

public interface EventRepository extends CrudRepository<Event, Long> {
}
