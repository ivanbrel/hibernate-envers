package com.itsupportme.demoenvers.model;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "invitation_events", schema = "model")
@Audited(withModifiedFlag = true)
public class InvitationEvent extends Event {

    @ManyToOne
    private User inviter;

    private String invitationEmail;

    public User getInviter() {
        return inviter;
    }

    public void setInviter(User inviter) {
        this.inviter = inviter;
    }

    public String getInvitationEmail() {
        return invitationEmail;
    }

    public void setInvitationEmail(String invitationEmail) {
        this.invitationEmail = invitationEmail;
    }
}