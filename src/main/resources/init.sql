CREATE SCHEMA IF NOT EXISTS logs;
CREATE SCHEMA IF NOT EXISTS model;

CREATE TABLE IF NOT EXISTS logs.events_aud (
    id bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint
);

CREATE TABLE IF NOT EXISTS logs.invitation_events_aud (
    id bigint NOT NULL,
    rev integer NOT NULL,
    invitation_email character varying(255),
    invitation_email_mod boolean,
    inviter_id bigint,
    inviter_mod boolean
);

CREATE TABLE IF NOT EXISTS logs.revinfo (
    rev integer NOT NULL,
    revtstmp bigint
);

CREATE TABLE IF NOT EXISTS logs.revision_entity (
    id integer NOT NULL,
    "timestamp" bigint NOT NULL,
    user_id integer NOT NULL,
    username character varying(255)
);

CREATE SEQUENCE IF NOT EXISTS logs.revision_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE IF NOT EXISTS logs.users_aud (
    id bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    email character varying(255),
    email_mod boolean,
    first_name character varying(255),
    first_name_mod boolean,
    last_name character varying(255),
    last_name_mod boolean
);

CREATE TABLE IF NOT EXISTS model.invitation_events (
    invitation_email character varying(255),
    id bigint NOT NULL,
    inviter_id bigint
);


CREATE TABLE IF NOT EXISTS model.users (
    id bigint NOT NULL,
    email character varying(255),
    first_name character varying(255),
    last_name character varying(255)
);


CREATE TABLE IF NOT EXISTS public.events (
    id bigint NOT NULL
);

CREATE SEQUENCE IF NOT EXISTS public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- ALTER TABLE ONLY logs.events_aud ADD CONSTRAINT events_aud_pkey PRIMARY KEY (id, rev);
--
-- ALTER TABLE ONLY logs.invitation_events_aud ADD CONSTRAINT invitation_events_aud_pkey PRIMARY KEY (id, rev);
--
-- ALTER TABLE ONLY logs.revinfo ADD CONSTRAINT revinfo_pkey PRIMARY KEY (rev);
--
-- ALTER TABLE ONLY logs.revision_entity ADD CONSTRAINT revision_entity_pkey PRIMARY KEY (id);
--
-- ALTER TABLE ONLY logs.users_aud ADD CONSTRAINT users_aud_pkey PRIMARY KEY (id, rev);
--
-- ALTER TABLE ONLY model.invitation_events ADD CONSTRAINT invitation_events_pkey PRIMARY KEY (id);
--
-- ALTER TABLE ONLY model.users ADD CONSTRAINT users_pkey PRIMARY KEY (id);
--
-- ALTER TABLE ONLY public.events ADD CONSTRAINT events_pkey PRIMARY KEY (id);
--
-- ALTER TABLE ONLY logs.invitation_events_aud ADD CONSTRAINT fkcdw4qdjowp3aja9e5gm984cn8 FOREIGN KEY (id, rev) REFERENCES logs.events_aud(id, rev);
--
-- ALTER TABLE ONLY logs.events_aud ADD CONSTRAINT fkduicdbr0inixi6tuv6847opw9 FOREIGN KEY (rev) REFERENCES logs.revision_entity(id);
--
-- ALTER TABLE ONLY logs.users_aud ADD CONSTRAINT fklr871cjoem85ixhng5gn2gwds FOREIGN KEY (rev) REFERENCES logs.revision_entity(id);
--
-- ALTER TABLE ONLY model.invitation_events ADD CONSTRAINT fk81cd64r99lm86sbw5yi6ceitg FOREIGN KEY (id) REFERENCES public.events(id);
--
-- ALTER TABLE ONLY model.invitation_events ADD CONSTRAINT fkf5dkk7eb0j25pvimwwvo2ctqx FOREIGN KEY (inviter_id) REFERENCES model.users(id);